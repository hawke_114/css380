###############HOMEWORK 2######################
##### CSEC-380/480 Advanced Security Automation - Ryan Haley####



import re
#1)	Using regular expressions, write code to match (and print to the user) the following:

#1.1) URL (http://www.depaul.edu AND https://depaul.edu)
s = "URL (http://www.depaul.edu AND https://depaul.edu)"
r = "https?://w?w?w?\.?depaul.edu"
print(re.findall(r,s))

#1.2) Credit Card Number (1234 5678 9012 3456)
s = "Credit Card Number (1234 5678 9012 3456)"
r = "\d{4}\ \d{4}\ \d{4}\ \d{4}"
print(re.findall(r,s))


#1.3) Email Addresses (rhaley@depaul.edu AND rhaley@mail.depaul.edu)
s = "Email Addresses (rhaley@depaul.edu AND rhaley@maildepaul.edu AND mdudlo@dudlo.net)"
#r = "\w+\@\w+\.?\.depaul.edu"
r = "\w+\@[\w+\.]?\w+\.\w+"
print(re.findall(r,s))



'''
#2)
On your pentest, you were able to compromise a webserver and dump password hashes. Unfortunatly, you can't crack them in their current format. You must remove invalid hashes in order for the program to run properly.
Write a program that will open a text document (BadHashes.txt in Resources file), and print all the valid hashes that have the following format:

username(up to 8 char),UID(up to 4 int),PermissionValue(up to 2 int),hash(in base64 - up to 32);

The file containes both good and bad hash examples.
Example hash:
Jsmith123,1001,10,KzEgcG9pbnQgaWYgeW91IGRlY29kZWQgdGhpcw==
'''
with open("BadHashes.txt",'r') as f:
        hashes = f.readlines()
r = "^\w{1,8}\,\d{1,4}\,\d{1,2}\,[A-Za-z0-9\+\/\=]{1,32}\;"

for i in hashes:
        print(re.findall(r,i))



#3)The following programs are not working correctly.  Find and resolve the error(s) so they work as intended.    Above each code snippet is a description of how the program should function. 

#3.1)	The following function takes a filename as a parameter and returns the number of words in the file.
def wordcount(fname):
	infile = open(fname,'r')
	s = infile.readline()
	infile.close()
	s = s.split()
	return len(s)
	
print(wordcount('file.txt'))
###3.2)	The following function takes a string as a parameter and returns a list containing
##all of the digits in the string.  The list contains integers rather than strings.
##If there are no digits in the string, an empty list is returned.
##If a digit appears multiple times, it is included in the list multiple times.
##
def findDigits(s):
    newLst=[]
    for char in s:
        for i in range (0,10):
            if str(i) == char:
                newLst.append(i)
    return newLst
print(findDigits('3 2 1 blastoff!'))
print(findDigits('Testing 1, testing 12, testing 123.'))
print(findDigits('There are no digits here'))

#Example output:
#>>>findDigits(‘3 2 1 blastoff!’)
#[3,2,1]
#>>> findDigits(‘Testing 1, testing 12, testing 123.’)
#[1,1,2,1,2,3]
#>>> findDigits(‘There are no digits here’)
#[]

###3.3)	The following function HALF takes a list of numbers and returns True of
#each element is exactly half the previous, and False otherwise.
###Test with the following:
a = [7,5,3,1] #– returns False
b = [4,3,2,1] #– returns False
c = [20,10,5] #– returns True


def HALF(nums):
    for i in range(0,len(nums)-1):
        if nums[i+1] == (nums[i]/2):
            pass
        else:
            return False
    return True

print(HALF(a))
print(HALF(b))
print(HALF(c))





